<?php 
require "connection.php";

$error="";

if(isset($_POST["login"])){
    $user=$_POST["username"];
    $pwd=sha1($_POST["password"]);

    $con=mysqli_connect(host,user,pwd,dbname);
    mysqli_query($con,"SET NAMES utf8");
    $sql="select * from login";
    $result=mysqli_query($con,$sql);

    while($row=mysqli_fetch_array($result)){
        $username=$row["username"];
        $password=$row["password"];
        if($username=$user && $password=$pwd){
            header("Location: database.php");
        }
        else{
            $error="Minden mező kitöltése kötelező!";
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="Description" content="Kedves családi vállalkozás, tejmenetes cukrászda."/>

  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">

    <link href="https://fonts.googleapis.com/css2?family=Cinzel:wght@400;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap" rel="stylesheet">

    <link rel="shortcut icon" type="image/x-icon" href="img/a.jpg"/>

    <link rel="stylesheet" href="css/admin.css">

    <title>Aurora cukrászda</title>

</head>
<body>
   

    <div class="wrapper p-5 mx-auto my-5">
            <div id="loginpage" class="text-center">
                <span>
                    <?php 
                        if(!empty($error)){
                            echo $error;
                        }
                    ?>
                <span>
                <h1 class="maintext mb-3">Jelentkezz be!</h1>
                <div class="col-4 mx-auto">
                    <form action="" method="post">
                        <label>Felhasználónév: </label>
                        <input type="text" name="username" placeholder="Felhasználó..." class="form-control my-3">

                        <label>Jelszó: </label>
                        <input type="password" name="password" placeholder="Jelszó..." class="form-control my-3">

                        <button type="submit" name="login" id="login" class="btn mt-2">Bejelentkezés</button>
                    </form>
                </div>
                   
            </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>

</body>
</html>