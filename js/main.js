const cookieContainer = document.querySelector(".cookie-container");
const cookieYes = document.querySelector(".cookie-yes");
const cookieNo = document.querySelector(".cookie-no");

cookieYes.addEventListener("click", () => {
  cookieContainer.classList.remove("active");
  localStorage.setItem("cookiesDisplayed", "true");
});
cookieNo.addEventListener("click", () => {
    cookieContainer.classList.remove("active");
});

setTimeout(() => {
  if (!localStorage.getItem("cookiesDisplayed")) {
    cookieContainer.classList.add("active");
  }
}, 2000);

$(window).scroll(function(){
    var navbar = $("#navbar");
    $(navbar).toggleClass("navigation", $(this).scrollTop() > 100)
})

$("#desc1, #desc2, #desc3, #desc4, #desc5, #desc6, #desc7, #desc8, #desc9, #desc10, #desc11, #desc12").hide();

$("#price1").on("click",function(){
  $("#desc1").fadeToggle("slow");
}) 
$("#price2").on("click",function(){
  $("#desc2").fadeToggle("slow");
})
$("#price3").on("click",function(){
  $("#desc3").fadeToggle("slow");
})
$("#price4").on("click",function(){
  $("#desc4").fadeToggle("slow");
})
$("#price5").on("click",function(){
  $("#desc5").fadeToggle("slow");
})
$("#price6").on("click",function(){
  $("#desc6").fadeToggle("slow");
})
$("#price7").on("click",function(){
  $("#desc7").fadeToggle("slow");
})
$("#price8").on("click",function(){
  $("#desc8").fadeToggle("slow");
})
$("#price9").on("click",function(){
  $("#desc9").fadeToggle("slow");
})
$("#price10").on("click",function(){
  $("#desc10").fadeToggle("slow");
})
$("#price11").on("click",function(){
  $("#desc11").fadeToggle("slow");
})
$("#price12").on("click",function(){
  $("#desc12").fadeToggle("slow");
})


