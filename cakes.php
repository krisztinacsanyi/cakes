<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="Description" content="Kedves családi vállalkozás, tejmenetes cukrászda."/>

  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">

    <link href="https://fonts.googleapis.com/css2?family=Cinzel:wght@400;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap" rel="stylesheet">

    <link rel="shortcut icon" type="image/x-icon" href="img/a.jpg"/>

    <link rel="stylesheet" href="css/style.css">

    <title>Aurora cukrászda</title>

</head>
<body>
    <a id="totop" href="#menu">▲ Fel</a>
    <div class="cookie-container">
        <p>
        Az aurora.hu sütiket használ a weboldal működtetése, 
        elemzése, használatának megkönnyítése és a releváns ajánlatok megjelenítése érdekében. 
        Ha le szeretnéd tiltani az összes sütit, válaszd a ’Nem fogadom el’ lehetőséget.
        Az alábbi linken elolvasható a(z)
            <a href="#">Adatvédelmi nyilatkozat</a> és a <a href="#">Sütik nyilatkozata</a>.
        </p>
        <button class="cookie-yes btn btn-light text-dark">
            Elfogadom
        </button>
        <button class="cookie-no btn btn-dark text-light">
            Nem fogadom el
        </button>
    </div>    

    <div id="menu">
        <?php require "nav.php";?>
    </div>

    <div class="wrapper">
        <div class="wrapper_bg">
            <div id="peoplepage" class="text-center">
                    <h1 class="subtext">Rendelhető tortáink</h1>
                    <p class="mainp">Egyedi rendeléseket telefonon veszünk fel</p>
            </div>
        </div>
    </div>

    <div class="cakes mt-5">
        <div class="container">
            <div class="row">
                <div class="cakebox col-xl-4 col-l-4 col-m-4 col-sm-7 mx-auto mb-4">
                    <img src="img/prod1.jpg" alt="">
                    <span class="p-3">
                        <h4>Gyümölcsös "cheesecake"</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, sequi.</p>
                        <h5 id="price1">Árak</h5>
                        <div id="desc1">
                            <ul>
                                <li>8 szelet: 	<strong>4 620 Ft</strong></li>
                                <li>12 szelet: 	<strong>6 600 Ft</strong></li>
                                <li>16 szelet: 	<strong>8 800 Ft</strong></li>
                            </ul>
                        </div>

                    </span>
                    
                </div>
                <div class="cakebox col-xl-4 col-l-4 col-m-4 col-sm-7 mx-auto mb-4">
                    <img src="img/prod2.jpg" alt="">
                    <span class="p-3">
                        <h4>Málnás-mákos torta</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, sequi.</p>
                        <h5 id="price2">Árak</h5>
                        <div id="desc2">
                            <ul>
                                <li>8 szelet: 	<strong>4 620 Ft</strong></li>
                                <li>12 szelet: 	<strong>6 600 Ft</strong></li>
                                <li>16 szelet: 	<strong>8 800 Ft</strong></li>
                            </ul>
                        </div>

                    </span>
                </div>
                <div class="cakebox col-xl-4 col-l-4 col-m-4 col-sm-7 mx-auto mb-4">
                    <img src="img/prod3.jpg" alt="">
                    <span class="p-3">
                        <h4>Barackos-"túrós" torta</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, sequi.</p>
                        <h5 id="price3">Árak</h5>
                        <div id="desc3">
                            <ul>
                                <li>8 szelet: 	<strong>4 620 Ft</strong></li>
                                <li>12 szelet: 	<strong>6 600 Ft</strong></li>
                                <li>16 szelet: 	<strong>8 800 Ft</strong></li>
                            </ul>
                        </div>

                    </span>
                </div>
            </div>
            <div class="row">
                <div class="cakebox col-xl-4 col-l-4 col-m-4 col-sm-7 mx-auto mb-4">
                    <img src="img/prod4.jpg" alt="">
                    <span class="p-3">
                        <h4>Erdei gyümölcsös torta</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, sequi.</p>
                        <h5 id="price4">Árak</h5>
                        <div id="desc4">
                            <ul>
                                <li>8 szelet: 	<strong>4 620 Ft</strong></li>
                                <li>12 szelet: 	<strong>6 600 Ft</strong></li>
                                <li>16 szelet: 	<strong>8 800 Ft</strong></li>
                            </ul>
                        </div>

                    </span>
                    
                </div>
                <div class="cakebox col-xl-4 col-l-4 col-m-4 col-sm-7 mx-auto mb-4">
                    <img src="img/prod5.jpg" alt="">
                    <span class="p-3">
                        <h4>Csokis-"tejszínes" torta</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, sequi.</p>
                        <h5 id="price5">Árak</h5>
                        <div id="desc5">
                            <ul>
                                <li>8 szelet: 	<strong>4 620 Ft</strong></li>
                                <li>12 szelet: 	<strong>6 600 Ft</strong></li>
                                <li>16 szelet: 	<strong>8 800 Ft</strong></li>
                            </ul>
                        </div>

                    </span>
                </div>
                <div class="cakebox col-xl-4 col-l-4 col-m-4 col-sm-7 mx-auto mb-4">
                    <img src="img/prod6.jpg" alt="">
                    <span class="p-3">
                        <h4>Eperkrémes torta</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, sequi.</p>
                        <h5 id="price6">Árak</h5>
                        <div id="desc6">
                            <ul>
                                <li>8 szelet: 	<strong>4 620 Ft</strong></li>
                                <li>12 szelet: 	<strong>6 600 Ft</strong></li>
                                <li>16 szelet: 	<strong>8 800 Ft</strong></li>
                            </ul>
                        </div>

                    </span>
                </div>
            </div>
            <div class="row">
                <div class="cakebox col-xl-4 col-l-4 col-m-4 col-sm-7 mx-auto mb-4">
                    <img src="img/prod7.jpg" alt="">
                    <span class="p-3">
                        <h4>Puha habos torta</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, sequi.</p>
                        <h5 id="price7">Árak</h5>
                        <div id="desc7">
                            <ul>
                                <li>8 szelet: 	<strong>4 620 Ft</strong></li>
                                <li>12 szelet: 	<strong>6 600 Ft</strong></li>
                                <li>16 szelet: 	<strong>8 800 Ft</strong></li>
                            </ul>
                        </div>

                    </span>
                    
                </div>
                <div class="cakebox col-xl-4 col-l-4 col-m-4 col-sm-7 mx-auto mb-4">
                    <img src="img/prod8.jpg" alt="">
                    <span class="p-3">
                        <h4>"Joghurtos" torta</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, sequi.</p>
                        <h5 id="price8">Árak</h5>
                        <div id="desc8">
                            <ul>
                                <li>8 szelet: 	<strong>4 620 Ft</strong></li>
                                <li>12 szelet: 	<strong>6 600 Ft</strong></li>
                                <li>16 szelet: 	<strong>8 800 Ft</strong></li>
                            </ul>
                        </div>

                    </span>
                </div>
                <div class="cakebox col-xl-4 col-l-4 col-m-4 col-sm-7 mx-auto mb-4">
                    <img src="img/prod9.jpg" alt="">
                    <span class="p-3">
                        <h4>Csokis-réteges torta</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, sequi.</p>
                        <h5 id="price9">Árak</h5>
                        <div id="desc9">
                            <ul>
                                <li>8 szelet: 	<strong>4 620 Ft</strong></li>
                                <li>12 szelet: 	<strong>6 600 Ft</strong></li>
                                <li>16 szelet: 	<strong>8 800 Ft</strong></li>
                            </ul>
                        </div>

                    </span>
                </div>
            </div>
            <div class="row">
                <div class="cakebox col-xl-4 col-l-4 col-m-4 col-sm-7 mx-auto mb-4">
                    <img src="img/prod10.jpg" alt="">
                    <span class="p-3">
                        <h4>Palacsintatorta</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, sequi.</p>
                        <h5 id="price10">Árak</h5>
                        <div id="desc10">
                            <ul>
                                <li>8 szelet: 	<strong>4 620 Ft</strong></li>
                                <li>12 szelet: 	<strong>6 600 Ft</strong></li>
                                <li>16 szelet: 	<strong>8 800 Ft</strong></li>
                            </ul>
                        </div>

                    </span>
                    
                </div>
                <div class="cakebox col-xl-4 col-l-4 col-m-4 col-sm-7 mx-auto mb-4">
                    <img src="img/prod11.jpg" alt="">
                    <span class="p-3">
                        <h4>Sacher torta</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, sequi.</p>
                        <h5 id="price11">Árak</h5>
                        <div id="desc11">
                            <ul>
                                <li>8 szelet: 	<strong>4 620 Ft</strong></li>
                                <li>12 szelet: 	<strong>6 600 Ft</strong></li>
                                <li>16 szelet: 	<strong>8 800 Ft</strong></li>
                            </ul>
                        </div>

                    </span>
                </div>
                <div class="cakebox col-xl-4 col-l-4 col-m-4 col-sm-7 mx-auto mb-4">
                    <img src="img/prod12.jpg" alt="">
                    <span class="p-3">
                        <h4>Karamellás torta</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, sequi.</p>
                        <h5 id="price12">Árak</h5>
                        <div id="desc12">
                            <ul>
                                <li>8 szelet: 	<strong>4 620 Ft</strong></li>
                                <li>12 szelet: 	<strong>6 600 Ft</strong></li>
                                <li>16 szelet: 	<strong>8 800 Ft</strong></li>
                            </ul>
                        </div>

                    </span>
                </div>
            </div>
        </div>
    </div>



    <div class="bottom copyright">
        <p class="mb-0 py-2 text-center"><span class="mb-0 mr-3 right">Telefonszám: <a href="tel:+3614567888">+36-1/456-78-88</a></span>© - Minden jog fenntartva!   <span class="mb-0 ml-3 left">E-mail: <a href="mailto:aurora@gmail.com">aurora@gmail.com</a></span></p> 
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>

</body>
</html>