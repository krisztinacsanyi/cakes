<?php 
require "connection.php";

$error="";

if(isset($_POST["login"])){

    $con=mysqli_connect(host,user,pwd,dbname);
    mysqli_query($con,"SET NAMES utf8");
    $sql="select * from rendelesek";
    $result=mysqli_query($con,$sql);

    while($row=mysqli_fetch_array($result)){
        $name=$row["name"];
        $email=$row["email"];
        $phone=$row["phone"];
        $cake=$row["select_cake"];
        $size=$row["select_size"];
        $date=$row["date"];
        $status=$row["statusz"];

        echo"
                    <div class='col-6 p-4 mx-auto mt-2 tablazat'>
                        <tr>
                            <td>".$name."</td>
                            <td>".$email."</td>
                            <td>".$phone."</td>
                            <td>".$cake."</td>
                            <td>".$size."</td>
                            <td>".$date." Ft</td>
                            <td>".$status." Ft</td>
                        </tr>
                    </div>
        ";
    }
}



?>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="Description" content="Kedves családi vállalkozás, tejmenetes cukrászda."/>

  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">

    <link href="https://fonts.googleapis.com/css2?family=Cinzel:wght@400;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap" rel="stylesheet">

    <link rel="shortcut icon" type="image/x-icon" href="img/a.jpg"/>

    <link rel="stylesheet" href="css/admin.css">

    <title>Aurora cukrászda</title>

</head>
<body>
   

    <div class="wrapper p-5 mx-auto my-5">
            <div id="loginpage" class="text-center">
                <h1 class="maintext mb-3">Rendelések</h1>
                <div class="mx-auto my-5 table-responsive">
                    <table class="table table-striped w-auto">
                        <tr>
                            <th>Név</th>
                            <th>E-mail cím</th>
                            <th>Telefonszám</th>
                            <th>Torta</th>
                            <th>Méret</th>
                            <th>Dátum</th>
                            <th colspan="2">Státusz</th>
                        </tr>
                        <form action="" method="post"></form>
                        
                   <?php
                    $con=mysqli_connect(host,user,pwd,dbname);
                    mysqli_query($con,"SET NAMES utf8");
                    $sql="select * from rendelesek";
                    $result=mysqli_query($con,$sql);
                    while($row=mysqli_fetch_array($result)){
                        $id=$row["id"];
                        $name=$row["nev"];
                        $email=$row["email"];
                        $phone=$row["telszam"];
                        $cake=$row["torta"];
                        $size=$row["meret"];
                        $date=$row["idopont"];
                        $status=$row["statusz"];
                
                        echo"
                                            <tr>
                                                <td><strong>".$name."</strong></td>
                                                <td>".$email."</td>
                                                <td>".$phone."</td>
                                                <td>".$cake."</td>
                                                <td>".$size."</td>
                                                <td>".$date."</td>
                                                <td class='status".$id."'>".$status."</td>
                                                <td><form method='POST'><button type='submit' name='mod' value='".$id."' class='btn change'>Kész</button></form></td>
                                            </tr>
                        ";
                    }
                    if(isset($_POST['mod'])){
                       
                        $sql="UPDATE rendelesek SET statusz='Kész' WHERE rendelesek.id = ".($_POST['mod'])."";
                        mysqli_query($con,$sql);
                    }
                   
                   ?>
                    </table>
                                       
                </div>
                   
            </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/admin.js"></script>

</body>
</html>