<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="Description" content="Kedves családi vállalkozás, tejmenetes cukrászda."/>

  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">

    <link href="https://fonts.googleapis.com/css2?family=Cinzel:wght@400;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap" rel="stylesheet">

    <link rel="shortcut icon" type="image/x-icon" href="img/a.jpg"/>

    <link rel="stylesheet" href="css/style.css">

    <title>Aurora cukrászda</title>

</head>
<body>
    <a id="totop" href="#menu">▲ Fel</a>
    <div class="cookie-container">
        <p>
        Az aurora.hu sütiket használ a weboldal működtetése, 
        elemzése, használatának megkönnyítése és a releváns ajánlatok megjelenítése érdekében. 
        Ha le szeretnéd tiltani az összes sütit, válaszd a ’Nem fogadom el’ lehetőséget.
        Az alábbi linken elolvasható a(z)
            <a href="#">Adatvédelmi nyilatkozat</a> és a <a href="#">Sütik nyilatkozata</a>.
        </p>
        <button class="cookie-yes btn btn-light text-dark">
            Elfogadom
        </button>
        <button class="cookie-no btn btn-dark text-light">
            Nem fogadom el
        </button>
    </div>    

    <div id="menu">
        <?php require "nav.php";?>
    </div>

    <div class="wrapper">
        <div class="wrapper_bg">
            <div id="peoplepage" class="text-center">
                    <h1 class="subtext">Eseményeink</h1>
                    <p class="mainp">Ezeket a tortákat készítettük</p>
            </div>
        </div>
    </div>
    <div class="events mt-5">
    <div class="container">
        <div class="box">
            <div class="content">
                <img src="img/cake1.jpg" alt="">
                <p>A szülinapos nagyon boldog volt</p>
            </div>
        </div>
        <div class="box">
            <div class="content">
                <img src="img/cake2.jpg" alt="">
                <p>Egészséges ünneplés</p>
            </div>
        </div>
        <div class="box">
            <div class="content">
                <img src="img/cake3.jpg" alt="">
                <p>Névnapi csodatorta</p>
            </div>
        </div>
        <div class="box">
            <div class="content">
                <img src="img/cake4.jpg" alt="">
                <p>Esküvőre készült</p>
            </div>
        </div>
        <div class="box">
            <div class="content">
                <img src="img/cake5.jpg" alt="">
                <p>Meglepi diplomaosztóra</p>
            </div>
        </div>
        <div class="box">
            <div class="content">
                <img src="img/cake6.jpg" alt="">
                <p>Cukor, glutén, tej és tojásmentes party</p>
            </div>
        </div>
        <div class="box">
            <div class="content">
                <img src="img/cake7.jpg" alt="">
                <p>A leggyakrabban rendelt tortánk évfordulóra</p>
            </div>
        </div>
        <div class="box">
            <div class="content">
                <img src="img/cake8.jpg" alt="">
                <p>Csokis tortát minden héten készítünk</p>
            </div>
        </div>
    </div>


    </div>
    

    <div class="bottom copyright">
        <p class="mb-0 py-2 text-center"><span class="mb-0 mr-3 right">Telefonszám: <a href="tel:+3614567888">+36-1/456-78-88</a></span>© - Minden jog fenntartva!   <span class="mb-0 ml-3 left">E-mail: <a href="mailto:aurora@gmail.com">aurora@gmail.com</a></span></p> 
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>

</body>
</html>