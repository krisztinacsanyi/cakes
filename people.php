<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="Description" content="Kedves családi vállalkozás, tejmenetes cukrászda."/>

  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">

    <link href="https://fonts.googleapis.com/css2?family=Cinzel:wght@400;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap" rel="stylesheet">

    <link rel="shortcut icon" type="image/x-icon" href="img/a.jpg"/>

    <link rel="stylesheet" href="css/style.css">

    <title>Aurora cukrászda</title>

</head>
<body>
    <a id="totop" href="#menu">▲ Fel</a>
    <div class="cookie-container">
        <p>
        Az aurora.hu sütiket használ a weboldal működtetése, 
        elemzése, használatának megkönnyítése és a releváns ajánlatok megjelenítése érdekében. 
        Ha le szeretnéd tiltani az összes sütit, válaszd a ’Nem fogadom el’ lehetőséget.
        Az alábbi linken elolvasható a(z)
            <a href="#">Adatvédelmi nyilatkozat</a> és a <a href="#">Sütik nyilatkozata</a>.
        </p>
        <button class="cookie-yes btn btn-light text-dark">
            Elfogadom
        </button>
        <button class="cookie-no btn btn-dark text-light">
            Nem fogadom el
        </button>
    </div>    

    <div id="menu">
        <?php require "nav.php";?>
    </div>

    <div class="wrapper">
        <div class="wrapper_bg">
            <div id="peoplepage" class="text-center">
                    <h1 class="subtext">Kik dolgoznak a háttérben?</h1>
                    <p class="mainp">Mi készítjük a rendeléseid</p>
            </div>
        </div>
    </div>

    <div class="container people">
            <div class="row">
                <div class="col-12 p-3 mb-4 mt-2">
                    <h2>Ismerd meg csapatunkat</h2>
                    <h4>Nálunk dolgoznak a legjobb arcok, és a legprofibb cukrászok</h4>
                </div>
            </div>
            <div class="row">
                <article class="col-10 mx-auto">
                    <img src="img/Ákos.png" alt="Ákos">
                    <h5 class="mt-5 mb-3">Ákos - Cukrász</h5>
                    <p class="px-3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis eveniet maxime corrupti suscipit delectus fuga unde odio, quaerat adipisci, impedit recusandae. Minus vitae nemo, ex magni laboriosam expedita placeat sint, repellat omnis pariatur sit nihil, enim ut eaque veniam perferendis id facilis consequatur dolore in maiores fuga natus! Eos, at.</p>
                </article>
            </div>
            <div class="row">
                <article class="col-10 mx-auto">
                    <img src="img/Lilla.png" alt="Lilla" class="right">
                    <h5 class="mt-5 mb-3">Lilla - Kreatív menedzser</h5>
                    <p class="px-3">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quis, nisi. Obcaecati exercitationem odit facilis dicta repudiandae doloribus, nisi dignissimos dolore inventore aspernatur eligendi eos suscipit vero earum deserunt ad consectetur praesentium velit nobis quos ipsa, enim corporis deleniti. Molestias at porro qui inventore corrupti similique facere velit dolor aperiam odio!.</p>
                </article>
            </div>
            <div class="row">
                <article class="col-10 mx-auto">
                    <img src="img/Mari.png" alt="Mari">
                    <h5 class="mt-5 mb-3">Mari - Business</h5>
                    <p class="px-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis voluptates neque ullam suscipit temporibus vero, quas deserunt expedita architecto distinctio et a esse sapiente harum non accusantium deleniti dicta consequatur molestias earum necessitatibus nihil quisquam. Commodi vitae vel reprehenderit repudiandae repellat voluptates earum, hic animi. Eligendi vitae quos laudantium animi?</p>
                </article>
            </div>
            <div class="row">
                <article class="col-10 mx-auto">
                    <img src="img/Márk.png" alt="Márk" class="right">
                    <h5 class="mt-5 mb-3">Márk - IT</h5>
                    <p class="px-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos deserunt a, praesentium, itaque in soluta nulla id eum aut adipisci doloribus doloremque. Necessitatibus quaerat unde repellat laboriosam, blanditiis, odio, tenetur consequuntur voluptate molestias fuga omnis. Vitae eum labore vero blanditiis commodi, accusamus eos vel quibusdam deserunt eligendi iusto, voluptates quasi.</p>
                </article>
            </div>
            <div class="row">
                <article class="col-10 mx-auto mb-5">
                    <img src="img/Szandi.png" alt="Szandi">
                    <h5 class="mt-5 mb-3">Szandi - Cukrász</h5>
                    <p class="px-3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet inventore explicabo nulla dolorem praesentium, beatae numquam neque adipisci autem! Doloribus, cumque sapiente aspernatur ipsum cum provident enim earum, aperiam quasi necessitatibus consectetur omnis sed laborum illum! Eius amet similique itaque ut molestias accusamus eligendi odit corrupti sint nisi? Veritatis, tempora!</p>
                </article>
            </div>
               
    </div>

    <div class="bottom copyright">
        <p class="mb-0 py-2 text-center"><span class="mb-0 mr-3 right">Telefonszám: <a href="tel:+3614567888">+36-1/456-78-88</a></span>© - Minden jog fenntartva!   <span class="mb-0 ml-3 left">E-mail: <a href="mailto:aurora@gmail.com">aurora@gmail.com</a></span></p> 
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>

</body>
</html>