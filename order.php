<?php
require "connection.php";
$error="";
$success="";

if(isset($_POST["send"])){
    $name=$_POST["name"];
    $email=$_POST["email"];
    $phone=$_POST["phone"];
    $cake=$_POST["select_cake"];
    $size=$_POST["select_size"];
    $date=$_POST["date"];

    if(empty($name)||empty($email)||empty($phone)||empty($date)){
        $error="Minden mező kitöltése kötelező!";
    }
    else{
        $con=mysqli_connect(host,user,pwd,dbname);
        mysqli_query($con,"SET NAMES utf8");
        $sql="insert into rendelesek(nev, email, telszam, torta, meret, idopont,statusz) values('$name','$email','$phone','$cake','$size','$date','Függőben')";
        mysqli_query($con,$sql);

        $success="Sikeres feltöltés!";
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="Description" content="Kedves családi vállalkozás, tejmenetes cukrászda."/>

  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">

    <link href="https://fonts.googleapis.com/css2?family=Cinzel:wght@400;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap" rel="stylesheet">

    <link rel="shortcut icon" type="image/x-icon" href="img/a.jpg"/>

    <link rel="stylesheet" href="css/style.css">

    <title>Aurora cukrászda</title>

</head>
<body>
    <a id="totop" href="#menu">▲ Fel</a>
    <div class="cookie-container">
        <p>
        Az aurora.hu sütiket használ a weboldal működtetése, 
        elemzése, használatának megkönnyítése és a releváns ajánlatok megjelenítése érdekében. 
        Ha le szeretnéd tiltani az összes sütit, válaszd a ’Nem fogadom el’ lehetőséget.
        Az alábbi linken elolvasható a(z)
            <a href="#">Adatvédelmi nyilatkozat</a> és a <a href="#">Sütik nyilatkozata</a>.
        </p>
        <button class="cookie-yes btn btn-light text-dark">
            Elfogadom
        </button>
        <button class="cookie-no btn btn-dark text-light">
            Nem fogadom el
        </button>
    </div>    

    <div id="menu">
        <?php require "nav.php";?>
    </div>

    <div class="wrapper">
        <div class="order_bg">
            <div id="peoplepage" class="text-center">
                    <h1 class="subtext">Rendeld meg a tortádat!</h1>
            </div>
        </div>
    </div>

    <div class="order">
        <div class="container pt-5 mb-5">
            <div class="row text-center">
                <div class="col-xl-3 col-l-3 col-m-3 col-sm-12 col-xs-12 side m-3 mx-auto">
                    <div class="sidebox p-3">
                        <p class="p-3 mb-3">Tortáinkat megtekintheted a <a href="cakes.php">Torták fülön!</a></p> 
                        <p class="mb-3">Árak:</p>
                        <ul class="p-0">
                            <li>8 szelet: 	<strong>4 620 Ft</strong></li>
                            <li>12 szelet: 	<strong>6 600 Ft</strong></li>
                            <li>16 szelet: 	<strong>8 800 Ft</strong></li>
                        </ul>
                    </div>
                </div>
                <div class="col-8 mx-auto order_form p-4">
                    <form action="" method="post" class="form-group col-9 mx-auto">
                            <h5 class="d-block text-danger">
                                <?php
                                    if(!$error==""){
                                            echo $error;
                                    }
                                ?>
                            </h5>
                            <h5 class="d-block text-success">
                                <?php
                                    if(!$success==""){
                                            echo $success;
                                    }
                                ?>
                            </h5>
                            <label>Név: </label>
                            <input name="name" type="text" placeholder="Kérlek adj meg egy nevet..." class="form-control my-3">
                            
                            <label>Email cím: </label>
                            <input type="email" name="email" placeholder="Kérlek add meg az e-mail címed..." class="form-control my-3">

                            <label>Telefonszám:</label>
                            <input name="phone" type="text" placeholder="Kérlek adj meg egy telefonszámot..." class="form-control my-3">
                           
                            <label>Torta fajta: </label>
                            <small class="form-text text-muted mb-3">Amennyiben több tortát szeretnél rendelni, töltsd ki újra ezt az űrlapot egyező névvel és e-mail címmel!</small>
                            <select class="form-control mb-3" name="select_cake">
                                <option>Gyümölcsös "cheesecake"</option>
                                <option>Málnás-mákos torta</option>
                                <option>Barackos-"túrós" torta</option>
                                <option>Erdei gyümölcsös torta</option>
                                <option>Csokis-"tejszínes" torta</option>
                                <option>Eperkrémes torta</option>
                                <option>Puha habos torta</option>
                                <option>"Joghurtos" torta</option>
                                <option>Csokis-réteges torta</option>
                                <option>Palacsintatorta</option>
                                <option>Sacher torta</option>
                                <option>Karamellás torta</option>
                            </select>
                        
                            <label>Szeletek száma: </label>
                            <select class="form-control my-3" name="select_size">
                                <option>8</option>
                                <option>12</option>
                                <option>16</option>
                            </select>
                            
                            <label>Időpont: </label>
                            <small class="form-text text-muted mb-3">A megadott dátum napján átvehető 13:00-tól</small>
                            <input type="date" name="date" class="form-control my-3">

                            <button type="submit" name="send" id="send" class="btn mt-2">Megrendelés</button>

                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="bottom copyright">
        <p class="mb-0 py-2 text-center"><span class="mb-0 mr-3 right">Telefonszám: <a href="tel:+3614567888">+36-1/456-78-88</a></span>© - Minden jog fenntartva!   <span class="mb-0 ml-3 left">E-mail: <a href="mailto:aurora@gmail.com">aurora@gmail.com</a></span></p> 
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    

</body>
</html>