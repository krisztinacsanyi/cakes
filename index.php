<!--Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>-->
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="Description" content="Kedves családi vállalkozás, tejmenetes cukrászda."/>

  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">

    <link href="https://fonts.googleapis.com/css2?family=Cinzel:wght@400;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap" rel="stylesheet">

    <link rel="shortcut icon" type="image/x-icon" href="img/a.jpg"/>

    <link rel="stylesheet" href="css/style.css">

    <title>Aurora cukrászda</title>

</head>
<body>
    <a id="totop" href="#menu">▲ Fel</a>
    <div class="cookie-container">
        <p>
        Az aurora.hu sütiket használ a weboldal működtetése, 
        elemzése, használatának megkönnyítése és a releváns ajánlatok megjelenítése érdekében. 
        Ha le szeretnéd tiltani az összes sütit, válaszd a ’Nem fogadom el’ lehetőséget.
        Az alábbi linken elolvasható a(z)
            <a href="#">Adatvédelmi nyilatkozat</a> és a <a href="#">Sütik nyilatkozata</a>.
        </p>
        <button class="cookie-yes btn btn-light text-dark">
            Elfogadom
        </button>
        <button class="cookie-no btn btn-dark text-light">
            Nem fogadom el
        </button>
    </div> 

    <div id="menu">
        <?php require "nav.php";?>
    </div>
    
    <section id="bg">
        <div class="container">
            <div class="row justify-content-center text-center">
                <div id="mainpage">
                    <h1 class="maintext">Tejmentes cukrászda</h1>
                    <p class="mainp">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem eos accusantium error ab iure velit id ipsam, sunt illum, doloribus esse, minima commodi inventore modi.</p>
                </div>
            </div>
        </div>
    </section>

   
<div class="justify-content-center text-center middle">
    <div class="container">
            <div class="row">
                <div class="col-12 p-3 mb-4">
                    <h2 class="subtext">Süteményt mindenkinek!</h2>
                    <h4>Cukrászatunkban mindenki talál a saját ízlésének, diétájának megfelelő süteményt.</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-l-4 col-sm-12 suti">
                        <img src="img/dairy-free.png" alt="Tejmentes sütemény" class="icons">
                        <h5>Tejmentes</h5>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ratione quo ipsam totam, culpa ea libero deleniti distinctio tempora excepturi deserunt?</p>
                </div>
                <div class="col-xl-4 col-l-4 col-sm-12 suti">
                        <img src="img/gluten-free.png" alt="Gluténmentes sütemény" class="icons">
                        <h5>Gluténmentes</h5>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium itaque quam quisquam explicabo perferendis saepe aliquam at excepturi reprehenderit qui.</p>
                </div>
                <div class="col-xl-4 col-l-4 col-sm-12 suti">
                        <img src="img/sugar-free.png" alt="Cukormentes sütemény" class="icons">
                        <h5>Cukormentes</h5>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod neque temporibus quia iste consequuntur? Consequuntur illo cum saepe enim minima.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-l-4 col-sm-12 suti">
                    <img src="img/weight.png" alt="Diétás sütemény" class="icons">
                    <h5>Diétás</h5>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Exercitationem aliquam adipisci maiores dicta veritatis magni, hic dignissimos asperiores tempore quis.</p>
                </div>
                <div class="col-xl-4 col-l-4 col-sm-12 suti">
                    <img src="img/nutrition.png" alt="Egészséges sütemény" class="icons">
                    <h5>Egészséges</h5>
                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi blanditiis sed repudiandae ipsam? Veritatis harum repudiandae vel neque temporibus hic?</p>
                </div>
                <div class="col-xl-4 col-l-4 col-sm-12 suti">
                    <img src="img/heart.png" alt="Finom sütemény" class="icons">
                    <h5>Finom</h5>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam quam delectus accusantium a tempora nam repellendus quibusdam blanditiis similique aspernatur?</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="about">
    <div class="container">
        <div class="row justify-content-center mx-auto">
            <div class="col-xl-5 col-lg-5 col-m-5 col-sm-12 col-xs-12 p-3 m-2 mt-5 mb-5 text-center box">
                <h4>Itt találsz meg minket:</h4>
                <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d673.8080666339508!2d19.057899529217504!3d47.50486770465195!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4741dc6cfa7f1e69%3A0x89d73f7260bfc74e!2sBudapest%2C%20Nagymez%C5%91%20u.%2027%2C%201065!5e0!3m2!1shu!2shu!4v1594653835872!5m2!1shu!2shu" frameborder="0" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                <h4>Nagymező u. 27, Budapest, 1065</h4>
            </div>
            <div class="col-xl-5 col-lg-5 col-m-5 col-sm-12 p-3 m-2 mt-5 mb-5 text-center box">
                <span class="contact">
                    <h4 class="m-3">Telefonszám: <a href="tel:+3614567888">+36-1/456-78-88</a></h4>

                    <h4 class="m-3 mb-5">E-mail: <a href="mailto:aurora@gmail.com">aurora@gmail.com</a></h4>

                    <h4 class="m-3">Nyitvatartás:</h4>
                    <p>Hétfő-Péntek: 8:00-20:00</p>
                    <p>Szombat-Vasárnap: 10:00-18:00</p>
                </span>
                
            </div>
                
    </div>
</div>
<div class="bottom copyright">
        <p class="mb-0 py-2 text-center">© - Minden jog fenntartva!</p>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="js/main.js"></script>

</body>
</html>